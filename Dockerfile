FROM ubuntu:jammy AS docker-cli

RUN apt-get update && \
    apt-get install --no-install-suggests --no-install-recommends --yes --quiet \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
RUN mkdir -m 0755 -p /etc/apt/keyrings && \
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt-get update && \
    apt-get install --no-install-suggests --no-install-recommends --yes --quiet docker-ce-cli

FROM ubuntu:jammy

LABEL org.opencontainers.image.source="https://gitlab.com/onez3ph/mam-seedbox-ip-updated"
LABEL org.opencontainers.image.base.name="ubuntu:jammy"
LABEL description="Update Seedbox IP for MAM"
LABEL version="0.0.1"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get install --no-install-suggests --no-install-recommends --yes --quiet \
    curl 
    
RUN rm -rf /var/lib/apt/lists/* /var/cache/apt/*
RUN apt-get clean

ENV TZ=Etc/UTC
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US:en
ENV LC_ALL=en_US.UTF-8

RUN sed -i "/${LANG}/s/^# //g" /etc/locale.gen && \
    locale-gen && \
    ln -fs "/usr/share/zoneinfo/${TZ}" /etc/localtime && \
    dpkg-reconfigure tzdata


ENV MAM_ID=
ENV UPDATE_URL=
ENV UPDATE_INTERVAL_MIN=60


COPY --from=docker-cli /usr/bin/docker /usr/bin/docker
COPY data/start.sh /start.sh

RUN chmod 750 /start.sh

CMD ["./start.sh"]