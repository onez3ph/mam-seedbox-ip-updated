#!/usr/bin/env bash

# File to store the previous IP address
ip_file="previous_ip.txt"

timestamp() {
    date '+%Y-%m-%d %H:%M:%S'
}

# Function to retrieve the public IP address
get_public_ip() {
    curl -s https://ipinfo.io/ip
}

# Function to compare the current IP with the previous one
ip_has_changed() {
    current_ip=$(get_public_ip)
    previous_ip=$(cat "$ip_file")

    if [ "$current_ip" != "$previous_ip" ]; then
        echo "$(timestamp) IP has changed from $previous_ip to $current_ip"
        echo "$(timestamp) $current_ip" > "$ip_file"
        curl -c ~/mam.cookies -b "mam_id=${MAM_ID}" ${UPDATE_URL}
    else
        echo "$(timestamp) IP has not changed: $current_ip"
    fi
}

# Create the IP file if it doesn't exist
touch "$ip_file"

# Convert minutes to seconds
interval=$((${UPDATE_INTERVAL_MIN} * 60))

# Run the IP check every specified interval
while true; do
    ip_has_changed
    sleep "$interval"
done
